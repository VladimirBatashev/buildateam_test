import axios from 'axios';

import { UserApiModel, UserListModel, UserKnowledge, UserQueryResult, UserDetailModel } from '@/models/UserModels';

export class UserService {
    getUsers(): Promise<UserQueryResult<UserListModel[]>> {
        return axios.get('http://localhost:3000/api/users')
            .then(function (response) {
                let users: UserApiModel[] = response.data;
                let listUserModels = users.map((user: UserApiModel): UserListModel => {
                    return {
                        age: user.age,
                        id: user.id,
                        name: user.name,
                        languages: user.knowledge.map((knowledge: UserKnowledge): string => {
                            return knowledge.language;
                        })
                    };
                });

                return {
                    success: true,
                    data: listUserModels
                }
            })
            .catch(function (error) {
                return {
                    success: false
                }
            });
    }

    getUserById(id: Number): Promise<UserQueryResult<UserDetailModel>> {
        return axios.get(`http://localhost:3000/api/user/${id}`)
            .then(function (response) {
                let user: UserApiModel = response.data;
                let userDetails: UserDetailModel = {
                    id: user.id,
                    age: user.age,
                    name: user.name,
                    country: user.city,
                    knowledge: user.knowledge
                };

                return {
                    success: true,
                    data: userDetails
                };
            })
            .catch(function (error) {
                return {
                    success: false
                }
            });
    }

    updateUserCountry(id: Number, country: string): Promise<UserQueryResult<{}>> {
        return axios.patch(`http://localhost:3000/api/user/${id}`, {city: country})
            .then(function (response) {
                return {
                    success: true
                };
            })
            .catch(function (error) {
                return {
                    success: false
                }
            });
    }
}
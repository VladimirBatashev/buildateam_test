export type UserQueryResult<T> = {
    success: boolean,
    data?: T
}

export type UserListModel = {
    id: number,
    name: string,
    age: number,
    languages: string[]
}

export type UserDetailModel = {
    id: number,
    name: string,
    age: number,
    country: string,
    knowledge: UserKnowledge[]
}

export type UserApiModel = {
    id: number,
    name: string,
    age: number,
    city: string,
    knowledge: UserKnowledge[]
}

export type UserKnowledge = {
    language: string,
    frameworks: string[]
}
import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from "vue-property-decorator";

import { UserService } from "../services/UserService";
import { ViewMode } from "@/common/ViewMode";
import { UserQueryResult } from '@/models/UserModels';

@Component
export class UserServiceComponentMixin extends Vue {
    @Prop({ default: () => new UserService(), type: UserService })
    _service!: UserService;

    _mode = ViewMode.Loading;
    _viewMode = ViewMode;

    _loadData<T>(loadPromise: Promise<UserQueryResult<T>>, successCallback: (result: UserQueryResult<T>) => void) {
        loadPromise
            .then((result: UserQueryResult<T>) => {
                if (result.success) {
                    successCallback(result);
                    this.$data._mode = this.$data._viewMode.Success;
                } else {
                    this.$data._mode = this.$data._viewMode.Error;
                }
            });
    }
}
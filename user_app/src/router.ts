import Vue from 'vue'
import Router from 'vue-router'
import UserList from './components/UserList.vue'
import UserDetails from './components/UserDetails.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'list ',
      component: UserList
    },
    {
      path: '/user/:userId',
      name: 'user',
      component: UserDetails
    }
  ]
})
